package com.onlinegym.users.routing

import com.onlinegym.users.handler.TrainersHandler
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

class TrainersRouting(
    private val trainersHandler: TrainersHandler
) {
    fun router(): RouterFunction<ServerResponse> = router {
        GET("trainer/subscribe_url", trainersHandler::getTrainerSubscribeUrl)
        GET("trainer/users", trainersHandler::getTrainerUsers)
        GET("trainer", trainersHandler::getTrainer)
        GET("subscribe/{trainerId}", trainersHandler::subscribeUserToTrainer)
    }
}
