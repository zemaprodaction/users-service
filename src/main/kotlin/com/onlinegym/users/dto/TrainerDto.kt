package com.onlinegym.users.dto

import com.onlinegym.users.entity.User

data class TrainerDto(
    val id: String,
    val userList: List<User>,
    val subscribeUrl: TrainerSubscription
)
