package com.onlinegym.users.dto

data class TrainerSubscription(val subscriptionUrl: String)

