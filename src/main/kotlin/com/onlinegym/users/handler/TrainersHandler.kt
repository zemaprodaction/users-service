package com.onlinegym.users.handler

import com.onlinegym.users.dto.TrainerSubscription
import com.onlinegym.users.service.TrainerService
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import reactor.core.publisher.Mono
import java.net.URI

class TrainersHandler(private val trainerService: TrainerService) {

    fun getTrainerSubscribeUrl(request: ServerRequest) =
        ok().body(
            trainerService.getSubscribeUrl(request.principal()),
            TrainerSubscription::class.java
        )

    fun getTrainerUsers(request: ServerRequest) =
        ok().body(
            trainerService.getSubscribeUrl(request.principal()),
            TrainerSubscription::class.java
        )

    fun getTrainer(request: ServerRequest) =
        ok().body(
            trainerService.getTrainerDto(request.principal()),
            TrainerSubscription::class.java
        )

    fun subscribeUserToTrainer(request: ServerRequest): Mono<ServerResponse> {
        return trainerService.subscribeUserToTrainer(request.principal(), request.pathVariable("trainerId")).flatMap {
            ServerResponse.temporaryRedirect(URI.create("/you_subscribed"))
                .build()
        }
    }
}
