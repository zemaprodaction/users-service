package com.onlinegym.users.service

import com.onlinegym.users.dto.TrainerDto
import com.onlinegym.users.dto.TrainerSubscription
import com.onlinegym.users.entity.Trainer
import com.onlinegym.users.entity.User
import reactor.core.publisher.Mono
import java.security.Principal

interface TrainerService {
    fun getSubscribeUrl(principal: Mono<out Principal>): Mono<TrainerSubscription>
    fun getSubscribeUrl(trainerId: String): Mono<TrainerSubscription>
    fun getTrainerDto(principal: Mono<out Principal>): Mono<TrainerDto>
    fun subscribeUserToTrainer(principal: Mono<out Principal>, trainerId: String): Mono<Trainer>
    fun saveTrainer(trainer: Trainer): Mono<Trainer>
    fun getTrainerDto(trainerId: String): Mono<TrainerDto>
    fun getTrainer(trainerId: String): Mono<Trainer>
    fun getTrainerOrCreateNew(trainerId: String): Mono<Trainer>
}
