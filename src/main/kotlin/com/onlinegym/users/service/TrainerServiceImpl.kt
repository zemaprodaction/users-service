package com.onlinegym.users.service

import com.onlinegym.users.dto.TrainerDto
import com.onlinegym.users.dto.TrainerSubscription
import com.onlinegym.users.entity.Trainer
import com.onlinegym.users.entity.User
import com.onlinegym.users.repository.TrainerRepository
import com.onlinegym.users.repository.UserRepository
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.security.Principal

class TrainerServiceImpl(private val trainerRepository: TrainerRepository, private val userRepository: UserRepository) : TrainerService {
    private val rootUrl = System.getenv("ROOT_URL") ?: "https://onlinegym.space"
    override fun getSubscribeUrl(principal: Mono<out Principal>): Mono<TrainerSubscription> =
        principal.map {
            getTrainerSubscription(it.name)
        }



    override fun getSubscribeUrl(trainerId: String): Mono<TrainerSubscription> =
            TrainerSubscription(rootUrl + "/api/users/subscribe/" + trainerId).toMono()

    override fun getTrainerDto(principal: Mono<out Principal>) =
        principal.flatMap {
            this.getTrainerDto(it.name)
        }


    override fun subscribeUserToTrainer(principal: Mono<out Principal>, trainerId: String) =
        principal.flatMap { userPrincipal ->
            val prefferedUserName = (userPrincipal as JwtAuthenticationToken).token.claims.get("preferred_username")
            userRepository.findById(userPrincipal.name).defaultIfEmpty(User(id = userPrincipal.name, preferredName = prefferedUserName as String))
                .flatMap {
                    val newTrainer = Trainer(trainerId, listOf(userPrincipal.name))
                    userRepository.save(it.copy(trainer = newTrainer.id)).flatMap {
                        saveTrainer(newTrainer)
                    }
                }

        }


    override fun getTrainerOrCreateNew(trainerId: String) =
        getTrainer(trainerId).flatMap {
            it?.toMono() ?: saveTrainer(Trainer(id = trainerId))
        }

    override fun saveTrainer(trainer: Trainer): Mono<Trainer> =
        trainerRepository.save(trainer)

    override fun getTrainer(trainerId: String): Mono<Trainer> =
        trainerRepository.findTrainerById(trainerId).switchIfEmpty(trainerRepository.save(Trainer(id = trainerId, emptyList())))



    override fun getTrainerDto(trainerId: String) =
        getTrainer(trainerId).flatMap {
            userRepository.findByIdList(it.userList).collectList().map { userList ->
                TrainerDto(it.id, userList, getTrainerSubscription(it.id))
            }
        }

    private fun getTrainerSubscription(trainerId: String) = TrainerSubscription("$rootUrl/api/users/subscribe/$trainerId")

}
