package com.onlinegym.users.repository

import com.onlinegym.users.entity.User
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface UserRepository {

    fun save(user: User): Mono<User>

    fun findById(userId: String): Mono<User>

    fun findByIdList(userId: List<String>): Flux<User>
}
