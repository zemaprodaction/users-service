package com.onlinegym.users.repository

import com.onlinegym.users.entity.Trainer
import reactor.core.publisher.Mono

interface TrainerRepository {

    fun save(trainer: Trainer): Mono<Trainer>

    fun findTrainerById(trainerId: String): Mono<Trainer>
}
