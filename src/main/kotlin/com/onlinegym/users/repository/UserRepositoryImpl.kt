package com.onlinegym.users.repository

import com.onlinegym.users.entity.User
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

class UserRepositoryImpl(private val template: ReactiveMongoTemplate) : UserRepository {
    override fun save(user: User) =
        template.save(user)


    override fun findById(userId: String) =
        template.findById(userId, User::class.java)

    override fun findByIdList(userId: List<String>): Flux<User> =
        Query(Criteria("id").`in`(userId)).let {
            template.find(it, User::class.java)
        }

}
