package com.onlinegym.users.repository

import com.onlinegym.users.entity.Trainer
import org.springframework.data.mongodb.core.ReactiveMongoTemplate

class TrainerRepositoryImpl(private val template: ReactiveMongoTemplate) : TrainerRepository {

    override fun save(trainer: Trainer) =
        template.save(trainer)


    override fun findTrainerById(trainerId: String) =
        template.findById(trainerId, Trainer::class.java)

}
