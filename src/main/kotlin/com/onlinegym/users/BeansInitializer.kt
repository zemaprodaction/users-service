package com.onlinegym.users

import com.onlinegym.users.handler.TrainersHandler
import com.onlinegym.users.repository.TrainerRepositoryImpl
import com.onlinegym.users.repository.UserRepositoryImpl
import com.onlinegym.users.routing.TrainersRouting
import com.onlinegym.users.service.TrainerServiceImpl
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.support.GenericApplicationContext
import org.springframework.context.support.beans

val beans = beans {
    bean<TrainerServiceImpl>()
    bean<TrainersHandler>()
    bean<TrainerRepositoryImpl>()
    bean<UserRepositoryImpl>()
    bean { TrainersRouting(ref()).router() }
}

class BeansInitializer : ApplicationContextInitializer<GenericApplicationContext> {
    override fun initialize(context: GenericApplicationContext) {
        beans.initialize(context)
    }

}


