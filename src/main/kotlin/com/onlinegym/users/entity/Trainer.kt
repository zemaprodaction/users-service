package com.onlinegym.users.entity

import org.springframework.data.annotation.Id

data class Trainer(
    @Id
    val id: String,
    val userList: List<String> = emptyList()
)
