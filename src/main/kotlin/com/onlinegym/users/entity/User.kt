package com.onlinegym.users.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef

data class User(
    @Id
    val id: String,
    val trainer: String? = null,
    val preferredName: String = ""
)
