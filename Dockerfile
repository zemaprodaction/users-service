FROM openjdk:15-slim as builder
WORKDIR users
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} users.jar
RUN java -Djarmode=layertools -jar users.jar extract

FROM adoptopenjdk:13-jre-hotspot
WORKDIR users
ENV JAVA_TOOL_OPTIONS -agentlib:jdwp=transport=dt_socket,address=*:5008,server=y,suspend=n
COPY --from=builder users/dependencies/ ./
COPY --from=builder users/spring-boot-loader/ ./
COPY --from=builder users/snapshot-dependencies/ ./
COPY --from=builder users/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
